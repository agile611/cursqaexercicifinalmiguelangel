package com.itnove.ba.opencart;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.MobileBrowserType;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by guillemhs on 2017-02-16.
 */
public class OpBaseSauceTest {
    public RemoteWebDriver driver;
    public WebDriverWait wait;
    public Actions hover;

    public int timeOut = 10;
    public String user_OK = "user";
    public String password_OK = "bitnami1";
    public String urlAdmin = "http://opencart.votarem.lu/admin/";
    public String urlOpencart = "http://opencart.votarem.lu";

    @BeforeClass
    public void setUp() throws MalformedURLException {
        String device = System.getProperty("device");
        // switch between diffrent browsers, e.g. iOS Safari or Android Chrome
        // let's use the os name to differentiate, because we only use default browser in that os
        if (device != null && device.equalsIgnoreCase("android")) {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability("deviceName", "Android Emulator");
            capabilities.setCapability("platformVersion", "5.1");
            capabilities.setCapability("browserName", MobileBrowserType.BROWSER);
            driver = new AndroidDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), capabilities);
        } else {
            DesiredCapabilities caps = DesiredCapabilities.iphone();
            caps.setCapability("appiumVersion", "1.7.1");
            caps.setCapability("deviceName", "iPhone 6 Plus Simulator");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("platformVersion", "10.0");
            caps.setCapability("platformName", "iOS");
            caps.setCapability("browserName", "Safari");
            driver = new IOSDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
        }
        wait = new WebDriverWait(driver, timeOut);
        hover = new Actions(driver);
/*
        driver.manage().deleteAllCookies();
        driver.manage().window().fullscreen();
        driver.manage().timeouts().pageLoadTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(timeOut, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(timeOut, TimeUnit.SECONDS);
*/
    }

    @AfterClass
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
