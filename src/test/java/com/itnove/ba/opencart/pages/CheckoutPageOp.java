package com.itnove.ba.opencart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class CheckoutPageOp {

    @FindBy(xpath = ".//*[@id='accordion']/div[1]/div[1]/h4/a")
    public WebElement step1;

    @FindBy(xpath = ".//*[@id='accordion']/div[2]/div[1]/h4")
    public WebElement step2;

    @FindBy(xpath = ".//*[@id='accordion']/div[3]/div[1]/h4")
    public WebElement step3;

    @FindBy(xpath = ".//*[@id='accordion']/div[4]/div[1]/h4")
    public WebElement step4;

    @FindBy(xpath = ".//*[@id='accordion']/div[5]/div[1]/h4")
    public WebElement step5;

    @FindBy(xpath = ".//*[@id='accordion']/div[6]/div[1]/h4")
    public WebElement step6;

    @FindBy(xpath = ".//*[@id='collapse-checkout-option']/div/div/div[1]/div[2]/label/input")
    public WebElement radioGuest;

    @FindBy(xpath = ".//*[@id='button-account']")
    public WebElement bContinueStep1;

    @FindBy(xpath = ".//*[@id='input-payment-firstname']")
    public WebElement tFirstName;

    @FindBy(xpath = ".//*[@id='input-payment-lastname']")
    public WebElement tLastName;

    @FindBy(xpath = ".//*[@id='input-payment-email']")
    public WebElement tEmail;

    @FindBy(xpath = ".//*[@id='input-payment-telephone']")
    public WebElement tTelefon;

    @FindBy(xpath = ".//*[@id='input-payment-address-1']")
    public WebElement tAdress1;

    @FindBy(xpath = ".//*[@id='input-payment-city']")
    public WebElement tCity;

    @FindBy(xpath = ".//*[@id='input-payment-postcode']")
    public WebElement tPostCode;

    @FindBy(xpath = ".//*[@id='input-payment-country']/option")
    public List<WebElement> cCountry;

    @FindBy(xpath = ".//*[@id='input-payment-zone']/option")
    public List<WebElement> cRegion;

    @FindBy(xpath = ".//*[@id='collapse-payment-address']/div/div[2]/label/input")
    public WebElement cCopyAdress;

    @FindBy(xpath = ".//*[@id='button-guest']")
    public WebElement bContinueStep2;

    @FindBy(xpath = ".//*[@id='input-shipping-firstname']")
    public WebElement tFirstNameS;

    @FindBy(xpath = ".//*[@id='button-guest-shipping']")
    public WebElement bContinueStep3;

    @FindBy(xpath = ".//*[@id='collapse-shipping-method']/div/p[4]/textarea")
    public WebElement tComments4;

    @FindBy(xpath = ".//*[@id='button-shipping-method']")
    public WebElement bContinueStep4;

    @FindBy(xpath = ".//*[@id='collapse-payment-method']/div/p[3]/textarea")
    public WebElement tComments5;

    @FindBy(xpath = ".//*[@id='collapse-payment-method']/div/div[2]/div/input[1]")
    public WebElement cTermsConditions;

    @FindBy(xpath = ".//*[@id='button-payment-method']")
    public WebElement bContinueStep5;

    @FindBy(xpath = ".//*[@id='button-confirm']")
    public WebElement bConfirm;



    // Comprobar que se ha cargado bien la pagina de Checkout
    public boolean isCheckoutPageLoaded(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(step1));
        return step1.isDisplayed();
    }

    public boolean isElementDisplayed(WebDriverWait wait, WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.isDisplayed();
    }

    public void clickElement(WebElement element) throws InterruptedException {
        element.click();
    }

    public boolean rellenarStep1(WebDriverWait wait) throws InterruptedException {
        radioGuest.click();
        bContinueStep1.click();
        wait.until(ExpectedConditions.visibilityOf(tFirstName));
        return tFirstName.isDisplayed();
    }

    public boolean rellenarStep2(WebDriver driver, WebDriverWait wait) throws InterruptedException {
        tFirstName.sendKeys("Pepe");
        tLastName.sendKeys("Viyuela");
        tEmail.sendKeys("inventado@gmail.com");
        tTelefon.sendKeys("600123456");
        tAdress1.sendKeys("C/ Roc Boronat, 117");
        tCity.sendKeys("Barcelona");
        tPostCode.sendKeys("08020");
        Thread.sleep(4000);
        rellenarCombo(driver, cCountry, "Spain", ".//*[@id='input-payment-country']/option[");
        Thread.sleep(2000);
        rellenarCombo(driver, cRegion, "Barcelona", ".//*[@id='input-payment-zone']/option[");
//        cCopyAdress.click();
        bContinueStep2.click();

        String valorCopyAdress = cCopyAdress.getAttribute("checked");
        if (valorCopyAdress != null) {
            wait.until(ExpectedConditions.visibilityOf(tComments4));
            return tComments4.isDisplayed();
        } else {
            wait.until(ExpectedConditions.visibilityOf(tFirstNameS));
            return tFirstNameS.isDisplayed();
        }
    }

    public boolean rellenarStep4(WebDriverWait wait) throws InterruptedException {
        tComments4.clear();
        tComments4.sendKeys("Comentario de prueba - Paso 4");
        bContinueStep4.click();
        wait.until(ExpectedConditions.visibilityOf(tComments5));
        return tComments5.isDisplayed();
    }

    public boolean rellenarStep5(WebDriverWait wait) throws InterruptedException {
        tComments5.clear();
        tComments5.sendKeys("Paso 5 - Comentario de prueba");
        cTermsConditions.click();
        bContinueStep5.click();
        wait.until(ExpectedConditions.visibilityOf(bConfirm));
        return bConfirm.isDisplayed();
    }

    public void rellenarStep6(WebDriverWait wait) throws InterruptedException {
        bConfirm.click();
    }

    public void rellenarCombo(WebDriver driver, List<WebElement> listaCombo, String valor, String myXpath) {
//System.out.println("Buscando valor " + valor + ".  Tam: " + listaCombo.size());
        WebElement option = null;
        for (int i=1; i<listaCombo.size(); i++) {
            option = driver.findElement(By.xpath(myXpath + (i + 1) + "]"));
            if ( option.getText().equals(valor)) {
//                System.out.println("Valor " + valor + " encontrado en la iteración " + i );
                option.click();
                break;
            }
        }
    }

    public CheckoutPageOp(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
