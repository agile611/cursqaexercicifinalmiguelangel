package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class GestioCategoriesPageOp {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='menu-catalog']/a")
    public WebElement mCatalog;

    @FindBy(xpath = ".//*[@id='collapse1']/li[1]/a")
    public WebElement mCategories;

    @FindBy(xpath = ".//*[@id='content']/div[2]/div/div[1]/h3")
    public WebElement tCategoryList;

    @FindBy(xpath = ".//*[@id='content']/div[1]/div/div/a[1]")
    public WebElement bAfegir;

    public boolean isElementDisplayed(WebDriverWait wait, WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.isDisplayed();
    }

    public void clickElement(WebElement element) throws InterruptedException {
        element.click();
    }

    public boolean llistatCategories(WebDriverWait wait) throws InterruptedException {
        mCatalog.click();
        mCategories.click();
        return isElementDisplayed(wait, tCategoryList);
    }

    public GestioCategoriesPageOp(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
