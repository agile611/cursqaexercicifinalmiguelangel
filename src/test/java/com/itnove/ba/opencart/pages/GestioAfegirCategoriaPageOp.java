package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class GestioAfegirCategoriaPageOp {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='input-name1']")
    public WebElement tCategoryName;

    @FindBy(xpath = ".//*[@id='language1']/div[2]/div/div/div[3]/div[3]")
    public WebElement tDescription;

    @FindBy(xpath = ".//*[@id='input-meta-title1']")
    public WebElement tMetaTagTitle;

    @FindBy(xpath = ".//*[@id='input-meta-description1']")
    public WebElement tMetaTagDescription;

    @FindBy(xpath = ".//*[@id='input-meta-keyword1']")
    public WebElement tMetaTagKeywords;

    @FindBy(xpath = ".//*[@id='content']/div[1]/div/div/button")
    public WebElement bSave;

    public boolean isElementDisplayed(WebDriverWait wait, WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.isDisplayed();
    }

    public void clickElement(WebElement element) throws InterruptedException {
        element.click();
    }

    public void afegirCategoria(String categoria) throws InterruptedException {
        tCategoryName.sendKeys(categoria);
//        tDescription.sendKeys("Description " + categoria);
        tMetaTagTitle.sendKeys("Meta Tag Title " + categoria);
        tMetaTagDescription.sendKeys("Meta Tag Description " + categoria);
        tMetaTagKeywords.sendKeys("Meta Tag Keywords " + categoria);
        bSave.click();
    }

    public GestioAfegirCategoriaPageOp(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
