package com.itnove.ba.opencart.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class GestioLlistatCategoriesPageOp {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='content']/div[2]/div[1]")
    public WebElement titleCategoryList;

    @FindBy(xpath = ".//*[@id='content']/div[1]/div/div/a[1]")
    public WebElement bAfegir;

    public String pathPaginador1 = ".//*[@id='content']/div[2]/div/div[2]/div/div[1]/ul/li[";
    public String pathPaginador2 = "]/a";
    @FindBy(xpath = ".//*[@id='content']/div[2]/div/div[2]/div/div[1]/ul/li")
    public List<WebElement> paginador;

    public String pathListCategorias1 = ".//*[@id='form-category']/div/table/tbody/tr[";
    public String pathListCategorias2 = "]/td[2]";

    @FindBy(xpath = ".//*[@id='form-category']/div/table/tbody/tr")
    public List<WebElement> listaCategorias;
    // .//*[@id='form-category']/div/table/tbody/tr[1]/td[2]

    public String pathListChecks2 = "]/td[1]/input";
    @FindBy(xpath = ".//*[@id='form-category']/div/table/tbody/tr[")
    public List<WebElement> listaChecks;

    @FindBy(xpath = ".//*[@id='content']/div[1]/div/div/button")
    public WebElement bDelete;


    public boolean isElementDisplayed(WebDriverWait wait, WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.isDisplayed();
    }

    public void clickElement(WebElement element) throws InterruptedException {
        element.click();
    }

    public int cercarCategoria(WebDriver driver, WebDriverWait wait, String categoria) throws InterruptedException {
        WebElement item = null;
        boolean encontrada = false;
        int posicion = 0;
        int paginacion = 0;

        for (int i=1; i<paginador.size(); i++) {
            posicion = 0;
            for (int j=1; j<=listaCategorias.size(); j++) {
                item = driver.findElement(By.xpath(pathListCategorias1 + j + pathListCategorias2));
                if (item.getText().equals(categoria)) {
                    encontrada = true;
                    paginacion = i;
                    posicion = j;
                    break;
                }
            }
            if (encontrada)
                break;

            item = driver.findElement(By.xpath(pathPaginador1 + (i+2) + pathPaginador2));
            item.click();
            wait.until(ExpectedConditions.visibilityOf(titleCategoryList));

        }
System.out.println("Paginación: " + paginacion);
System.out.println("Posición: " + posicion);

        return posicion;
    }

    public int cercarCategoriaPaginada(WebDriver driver, WebDriverWait wait, String categoria) throws InterruptedException {
        WebElement item = null;
        int posicion = 0;

        for (int j=1; j<=listaCategorias.size(); j++) {
            item = driver.findElement(By.xpath(pathListCategorias1 + j + pathListCategorias2));
            if (item.getText().equals(categoria)) {
                posicion = j;
                break;
            }
        }

        return posicion;
    }

    public void clickCategoriaAfegida(WebDriver driver, int posicion) throws InterruptedException {
        WebElement checkCategoria = driver.findElement(By.xpath(pathListCategorias1 + posicion + pathListChecks2));
        checkCategoria.click();
    }

    public void deleteCategoria(WebDriver driver) throws InterruptedException {
        bDelete.click();
        Alert alert = driver.switchTo().alert();
        alert.accept();
        driver.switchTo().defaultContent();
    }

    public GestioLlistatCategoriesPageOp(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
