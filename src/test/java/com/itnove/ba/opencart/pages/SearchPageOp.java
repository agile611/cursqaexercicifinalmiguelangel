package com.itnove.ba.opencart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class SearchPageOp {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='search']/input")
    public WebElement searchBox;

    @FindBy(xpath = ".//*[@id='search']/span/button")
    public WebElement iconoLupa;

    @FindBy(xpath = ".//*[@id='content']/h1")
    public WebElement textListSearch;

    // Comprobamos que el dashboard principal esta cargado, con la ventana emergente que se ha de cerrar
    public boolean isListLoaded(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(textListSearch));
        return textListSearch.isDisplayed();
    }

    // Comprobar que se ha cargado bien la pagina de búsqueda principal
    public boolean isSearchPageLoaded(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(searchBox));
        return searchBox.isDisplayed();
    }

    public void search(WebDriver driver, WebDriverWait wait, Actions hover, String keyword) throws InterruptedException {
        wait.until(ExpectedConditions.visibilityOf(searchBox));
        searchBox.clear();
        searchBox.sendKeys(keyword);
        iconoLupa.click();
    }

    public SearchPageOp(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
