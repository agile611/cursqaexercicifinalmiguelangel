package com.itnove.ba.opencart.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class GestioAfegirCategoriaPageSauceOp {

    private WebDriver driver;

    @FindBy(xpath = "id('input-name1')")
    public WebElement tCategoryName;

    @FindBy(xpath = "id('language1')/div[2]/div[1]/div[1]/div[3]/div[2]")
    public WebElement tDescription;

    @FindBy(xpath = "id('input-meta-title1')")
    public WebElement tMetaTagTitle;

    @FindBy(xpath = "id('input-meta-description1')")
    public WebElement tMetaTagDescription;

    @FindBy(xpath = "id('input-meta-keyword1')")
    public WebElement tMetaTagKeywords;

    @FindBy(xpath = "id('content')/div[1]/div[1]/div[1]/button[1]")
    public WebElement bSave;

    public boolean isElementDisplayed(WebDriverWait wait, WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.isDisplayed();
    }

    public void clickElement(WebElement element) throws InterruptedException {
        element.click();
    }

    public void afegirCategoria(String categoria, WebDriver driver) throws InterruptedException {
//        tDescription.sendKeys("Description " + categoria);
        tMetaTagKeywords.sendKeys("Meta Tag Keywords " + categoria);
        tMetaTagDescription.sendKeys("Meta Tag Description " + categoria);
        tMetaTagTitle.sendKeys("Meta Tag Title " + categoria);
        tCategoryName.sendKeys(categoria);
//        JavascriptExecutor jse = (JavascriptExecutor)driver;
//        jse.executeScript("window.scrollBy(0,-250)", "");
        bSave.click();
        bSave.click();
    }

    public GestioAfegirCategoriaPageSauceOp(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
