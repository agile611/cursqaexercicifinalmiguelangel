package com.itnove.ba.opencart.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class CheckoutOrderOkPageOp {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='content']/div/div/a")
    public WebElement bContinue;

    public boolean isElementDisplayed(WebDriverWait wait, WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.isDisplayed();
    }

    public void clickElement(WebElement element) throws InterruptedException {
        element.click();
    }

    public CheckoutOrderOkPageOp(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
