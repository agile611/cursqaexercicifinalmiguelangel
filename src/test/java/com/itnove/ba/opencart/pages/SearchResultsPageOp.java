package com.itnove.ba.opencart.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class SearchResultsPageOp {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='content']/h1")
    public WebElement searchTitle;

    @FindBy(xpath = ".//*[@id='input-search']")
    public WebElement searchFieldMain;

    @FindBy(xpath = ".//*[@id='content']/div[3]/div[1]/div/div[1]/a/img")
    public WebElement results;

    @FindBy(xpath = ".//*[@id='content']/p[2]")
    public WebElement noResults;

    @FindBy(xpath = ".//*[@id='content']/div[3]/div[1]/div/div[2]/div[2]/button[1]")
    public WebElement firstProduct;

    @FindBy(xpath = ".//*[@id='cart']/button/span")
    public WebElement buttonShoppingCart;

    @FindBy(xpath = ".//*[@id='cart']/ul/li[2]/div/p/a[2]/strong")
    public WebElement checkout;

    // Comprobar que se ha cargado el listado correspondiente a la búsqueda
    public boolean isSearchResultsPageLoaded(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(searchTitle));
        return searchTitle.isDisplayed();
    }


    public String isSearchKeywordCorrect(){
        return searchFieldMain.getAttribute("value");
    }

    public boolean isNoSearchResultsDisplayed(WebDriverWait wait){
        wait.until(ExpectedConditions.visibilityOf(noResults));
        return noResults.isDisplayed();
    }

    public boolean isElementDisplayed(WebDriverWait wait, WebElement element){
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.isDisplayed();
    }

    public void clickOnFirstResult(WebDriverWait wait) throws InterruptedException {
        results.click();
    }

    public void clickElement(WebElement element) throws InterruptedException {
        element.click();
    }

    public SearchResultsPageOp(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
