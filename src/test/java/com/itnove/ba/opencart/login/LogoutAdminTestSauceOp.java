package com.itnove.ba.opencart.login;

import com.itnove.ba.opencart.OpBaseSauceTest;
import com.itnove.ba.opencart.OpBaseTest;
import com.itnove.ba.opencart.pages.DashboardAdminPageOp;
import com.itnove.ba.opencart.pages.DashboardAdminPageSauceOp;
import com.itnove.ba.opencart.pages.LoginAdminPageOp;
import com.itnove.ba.opencart.pages.LoginAdminPageSauceOp;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class LogoutAdminTestSauceOp extends OpBaseSauceTest {

    @Test
    public void testApp() throws InterruptedException {
        //Accedir a pagina
        driver.navigate().to(urlAdmin);

        LoginAdminPageSauceOp loginPage = new LoginAdminPageSauceOp(driver);
        loginPage.login(user_OK, password_OK);
        DashboardAdminPageSauceOp dashboardAdminPageOp = new DashboardAdminPageSauceOp(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardAdminPageOp.isDashboardLoaded(driver,wait));
        assertTrue(dashboardAdminPageOp.clickOnIconX());

        dashboardAdminPageOp.logout();
        loginPage = new LoginAdminPageSauceOp(driver);
        assertTrue(loginPage.isLoginButtonPresent(driver, wait));
        Thread.sleep(2000);

    }
}
