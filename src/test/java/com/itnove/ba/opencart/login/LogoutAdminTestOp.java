package com.itnove.ba.opencart.login;

import com.itnove.ba.opencart.OpBaseTest;
import com.itnove.ba.opencart.pages.DashboardAdminPageOp;
import com.itnove.ba.opencart.pages.LoginAdminPageOp;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class LogoutAdminTestOp extends OpBaseTest {

    @Test
    public void testApp() throws InterruptedException {
        //Accedir a pagina
        driver.navigate().to(urlAdmin);

        LoginAdminPageOp loginPage = new LoginAdminPageOp(driver);
        loginPage.login(user_OK, password_OK);
        DashboardAdminPageOp dashboardAdminPageOp = new DashboardAdminPageOp(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardAdminPageOp.isDashboardLoaded(driver,wait));
        assertTrue(dashboardAdminPageOp.clickOnIconX());

        dashboardAdminPageOp.logout();
        loginPage = new LoginAdminPageOp(driver);
        assertTrue(loginPage.isLoginButtonPresent(driver, wait));
        Thread.sleep(2000);

    }
}
