package com.itnove.ba.opencart.login;


import com.itnove.ba.opencart.OpBaseSauceTest;
import com.itnove.ba.opencart.pages.LoginAdminPageOp;
import com.itnove.ba.opencart.pages.LoginAdminPageSauceOp;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class LoginAdminIncorrecteTestSauceOp extends OpBaseSauceTest {

    public void checkErrors(String user, String passwd){
        // S'introdueix l'usuari.
        // S'introdueix la contrasenya.
        // Es clicka  al botó de login. 
        LoginAdminPageSauceOp loginPage = new LoginAdminPageSauceOp(driver);
        loginPage.login(user, passwd);
        //Comprovar l'error
        assertTrue(loginPage.isErrorMessagePresent(driver, wait));
        assertTrue(loginPage.errorMessageDisplayed().contains("No match for Username and/or Password."));
    }

    @Test
    public void testApp() throws InterruptedException {
        //Accedir a pagina
        driver.navigate().to(urlAdmin);

        //User ok passw KO
        checkErrors("user","nami");
        //User KO passwd OK
        checkErrors("resu","bitnami1");
        //User KO passwd KO
        checkErrors("resu","nami");
    }
}
