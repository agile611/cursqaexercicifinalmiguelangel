package com.itnove.ba.opencart.search;

import com.itnove.ba.opencart.OpBaseTest;
import com.itnove.ba.opencart.pages.SearchResultsPageOp;
import com.itnove.ba.opencart.pages.SearchPageOp;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class SearchCorrecteTestOp extends OpBaseTest {

    @Test
    public void testApp() throws InterruptedException {
        String keyword = "mac";
        //Accedir a pagina
        driver.navigate().to(urlOpencart);

        SearchPageOp mainPage = new SearchPageOp(driver);
        mainPage.search(driver,wait,hover,keyword);

        SearchResultsPageOp searchResultsPage = new SearchResultsPageOp(driver);
        assertTrue(searchResultsPage.isSearchResultsPageLoaded(wait));
        // Nos aseguramos que la busqueda realizada es con la palabra introducida
        assertEquals(searchResultsPage.isSearchKeywordCorrect(),keyword);
        // Compribamos que no hay resultados en la busqueda realizada
        searchResultsPage.clickOnFirstResult(wait);
        assertTrue(driver.getPageSource().toUpperCase().contains(keyword.toUpperCase()));

    }
}
