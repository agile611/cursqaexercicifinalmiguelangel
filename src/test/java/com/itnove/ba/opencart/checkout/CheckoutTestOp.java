package com.itnove.ba.opencart.checkout;

import com.itnove.ba.opencart.OpBaseTest;
import com.itnove.ba.opencart.pages.CheckoutOrderOkPageOp;
import com.itnove.ba.opencart.pages.CheckoutPageOp;
import com.itnove.ba.opencart.pages.SearchResultsPageOp;
import com.itnove.ba.opencart.pages.SearchPageOp;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CheckoutTestOp extends OpBaseTest {

    @Test
    public void testApp() throws InterruptedException {
        String keyword = "mac";
        //Accedir a pagina
        driver.navigate().to(urlOpencart);

        SearchPageOp mainPage = new SearchPageOp(driver);
        mainPage.search(driver,wait,hover,keyword);

        SearchResultsPageOp searchResultsPage = new SearchResultsPageOp(driver);
        assertTrue(searchResultsPage.isSearchResultsPageLoaded(wait));
        // Nos aseguramos que la busqueda realizada es con la palabra introducida
        assertEquals(searchResultsPage.isSearchKeywordCorrect(),keyword);

        // Añadimos el primer elemento de la lista al carrito de la compra
        searchResultsPage.clickElement(searchResultsPage.firstProduct);
        Thread.sleep(2000);

        // Hacemos click en el boton del carrito de la compra.
        searchResultsPage.clickElement(searchResultsPage.buttonShoppingCart);
        // Hacemos click en el área de checkout

        Thread.sleep(1000);
        searchResultsPage.clickElement(searchResultsPage.checkout);
        Thread.sleep(2000);

        CheckoutPageOp checkoutPage = new CheckoutPageOp(driver);
        assertTrue(checkoutPage.isCheckoutPageLoaded(wait));

        assertTrue(checkoutPage.rellenarStep1(wait));
        Thread.sleep(2000);

        assertTrue(checkoutPage.rellenarStep2(driver, wait));
        Thread.sleep(2000);

        assertTrue(checkoutPage.rellenarStep4(wait));
        Thread.sleep(2000);

        assertTrue(checkoutPage.rellenarStep5(wait));
        Thread.sleep(2000);

        checkoutPage.rellenarStep6(wait);
        Thread.sleep(2000);

        CheckoutOrderOkPageOp checkoutOrderOK = new CheckoutOrderOkPageOp(driver);
        assertTrue(checkoutOrderOK.isElementDisplayed(wait, checkoutOrderOK.bContinue));
        Thread.sleep(2000);

        checkoutOrderOK.clickElement(checkoutOrderOK.bContinue);
        mainPage = new SearchPageOp(driver);
        assertTrue(mainPage.isSearchPageLoaded(wait));
        Thread.sleep(3000);

    }
}
