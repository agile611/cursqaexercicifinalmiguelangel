package com.itnove.ba.opencart.gestio;

import com.itnove.ba.opencart.OpBaseTest;
import com.itnove.ba.opencart.pages.*;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class GestioCategoriesTestOp extends OpBaseTest {

    @Test
    public void testApp() throws InterruptedException {
        //Accedir a pagina
        driver.navigate().to(urlAdmin);
        String categoria = "zzBendita categoria";

        LoginAdminPageOp loginPage = new LoginAdminPageOp(driver);
        loginPage.login(user_OK, password_OK);

        DashboardAdminPageOp dashboardAdminPageOp = new DashboardAdminPageOp(driver);
        assertTrue(dashboardAdminPageOp.isDashboardLoaded(driver,wait));
        assertTrue(dashboardAdminPageOp.clickOnIconX());

        GestioCategoriesPageOp gestioCategories = new GestioCategoriesPageOp(driver);
        gestioCategories.llistatCategories(wait);
        gestioCategories.clickElement(gestioCategories.bAfegir);

        // Cumplimentamos los campos de la categoria a añadir
        GestioAfegirCategoriaPageOp afegirCategoria = new GestioAfegirCategoriaPageOp(driver);
        afegirCategoria.isElementDisplayed(wait, afegirCategoria.tCategoryName);
        afegirCategoria.afegirCategoria(categoria);

        // Comprobamos que estamos en el listado tras haber añadido una categoria
        GestioLlistatCategoriesPageOp llistatCategories = new GestioLlistatCategoriesPageOp(driver);
        llistatCategories.isElementDisplayed(wait, llistatCategories.titleCategoryList);

        // Buscamos en el listado paginado donde se encuentra la categoria creada, para poder eliminarla
        int posicion = llistatCategories.cercarCategoria(driver, wait, categoria);
        assertTrue(posicion>0);

        llistatCategories.clickCategoriaAfegida(driver, posicion);
        llistatCategories.deleteCategoria(driver);

        llistatCategories = new GestioLlistatCategoriesPageOp(driver);
        llistatCategories.isElementDisplayed(wait, llistatCategories.titleCategoryList);

        assertTrue(llistatCategories.cercarCategoriaPaginada(driver, wait, categoria) == 0);
        Thread.sleep(3000);
    }
}
