package com.itnove.ba.opencart.gestio;

import com.itnove.ba.opencart.OpBaseSauceTest;
import com.itnove.ba.opencart.pages.*;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class GestioCategoriesTestSauceOp extends OpBaseSauceTest {

    @Test
    public void testApp() throws InterruptedException {
        //Accedir a pagina
        driver.navigate().to(urlAdmin);
        String categoria = "aaBendita categoria";

        LoginAdminPageSauceOp loginPage = new LoginAdminPageSauceOp(driver);
        loginPage.login(user_OK, password_OK);

        DashboardAdminPageSauceOp dashboardAdminPageOp = new DashboardAdminPageSauceOp(driver);
        assertTrue(dashboardAdminPageOp.isDashboardLoaded(driver,wait));
        assertTrue(dashboardAdminPageOp.clickOnIconX());

        GestioCategoriesPageSauceOp gestioCategories = new GestioCategoriesPageSauceOp(driver);
        gestioCategories.llistatCategories(wait);
        gestioCategories.clickElement(gestioCategories.bAfegir);

        Thread.sleep(5000);

        // Cumplimentamos los campos de la categoria a añadir
        GestioAfegirCategoriaPageSauceOp afegirCategoria = new GestioAfegirCategoriaPageSauceOp(driver);
        afegirCategoria.isElementDisplayed(wait, afegirCategoria.tCategoryName);
        afegirCategoria.afegirCategoria(categoria, driver);
        // Comprobamos que estamos en el listado tras haber añadido una categoria
        GestioLlistatCategoriesPageSauceOp llistatCategories = new GestioLlistatCategoriesPageSauceOp(driver);
        llistatCategories.isElementDisplayed(wait, llistatCategories.titleCategoryList);

        // Buscamos en el listado paginado donde se encuentra la categoria creada, para poder eliminarla
        int posicion = llistatCategories.cercarCategoria(driver, wait, categoria);
        assertTrue(posicion>0);

        llistatCategories.clickCategoriaAfegida(driver, posicion);
        llistatCategories.deleteCategoria(driver);

        llistatCategories = new GestioLlistatCategoriesPageSauceOp(driver);
        llistatCategories.isElementDisplayed(wait, llistatCategories.titleCategoryList);

        assertTrue(llistatCategories.cercarCategoriaPaginada(driver, wait, categoria) == 0);
        Thread.sleep(3000);

    }
}
