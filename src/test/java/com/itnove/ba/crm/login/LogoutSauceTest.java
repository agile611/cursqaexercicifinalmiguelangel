package com.itnove.ba.crm.login;

import com.itnove.ba.crm.CrmBaseSauceBrowserTest;
import com.itnove.ba.crm.pages.DashboardSaucePage;
import com.itnove.ba.crm.pages.LoginSaucePage;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class LogoutSauceTest extends CrmBaseSauceBrowserTest {

    @Test
    public void testApp() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        LoginSaucePage loginPage = new LoginSaucePage(driver);
        loginPage.login("user","bitnami");
        DashboardSaucePage dashboardPage = new DashboardSaucePage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.logout(driver,wait,hover);
        assertTrue(loginPage.isLoginButtonPresent(driver,wait));
    }
}
