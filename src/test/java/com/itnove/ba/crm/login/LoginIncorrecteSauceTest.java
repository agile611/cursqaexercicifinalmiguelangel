package com.itnove.ba.crm.login;


import com.itnove.ba.crm.CrmBaseSauceBrowserTest;
import com.itnove.ba.crm.pages.LoginSaucePage;
import org.testng.annotations.Test;


import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LoginIncorrecteSauceTest extends CrmBaseSauceBrowserTest {

    public void checkErrors(String user, String passwd){
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya incorrecte.
        // Es clicka  al botó de login. 
        LoginSaucePage loginPage = new LoginSaucePage(driver);
        loginPage.login(user, passwd);
        //Comprovar l'error
        assertTrue(loginPage.isErrorMessagePresent(driver, wait));
        assertEquals(loginPage.errorMessageDisplayed(),
                "You must specify a valid username and password.");
    }

    @Test
    public void testApp() throws InterruptedException {
        //User ok passw KO
        checkErrors("user","nami");
        //User KO passwd OK
        checkErrors("resu","bitnami");
        //User KO passwd KO
        checkErrors("resu","nami");
    }
}
