package com.itnove.ba.crm.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

/**
 * Created by guillem on 01/03/16.
 */
public class DashboardSaucePage {

    private WebDriver driver;

    @FindBy(xpath = "id('quickcreatetop')/ul[1]/li[1]/a[1]")
    public WebElement createAccountLink;

    @FindBy(xpath = "id('quickcreatetop')/ul[1]/li[5]/a[1]")
    public WebElement createDocumentLink;

    @FindBy(xpath = "id('quickcreatetop')/a[1]")
    public WebElement createButton;

    @FindBy(xpath = "id('xstab0')[1]")
    public WebElement suiteCrmDashboard;

    @FindBy(xpath = "id('usermenucollapsed')[1]")
    public WebElement icono;

    @FindBy(xpath = ".//*[@id='tab-actions']/a")
    public WebElement actions;

    @FindBy(xpath = ".//*[@id='tab-actions']/ul/li[2]/input")
    public WebElement addTab;

    @FindBy(xpath = ".//*[@id='pagecontent']/div[3]/div/div/div[3]/button[2]")
    public WebElement addTabWidgetButton;

    @FindBy(xpath = "(.//*[@id='globalLinks']/ul)[2]")
    public WebElement menuIcono;

    @FindBy(xpath = "id('logout_link')[1]")
    public WebElement logout;

    @FindBy(xpath = "id('searchbutton')[1]")
    public WebElement lupa;

    @FindBy(xpath = "id('query_string')[1]")
    public WebElement searchBox;

    @FindBy(xpath = "(id('searchformdropdown')/div[1]/span[1]/button[1])[1]")
    public WebElement iconoLupa;

    public void createButtonClick(Actions hover) throws InterruptedException {

        hover.moveToElement(createButton)
                .moveToElement(createButton)
                .click().build().perform();

/*
        hover.moveToElement(createButton).build().perform();
        createButton.click();
*/
    }

    public void clickOnCreateAccountLink(Actions hover) throws InterruptedException {
        hover.moveToElement(createButton)
                .moveToElement(createAccountLink)
                .click().build().perform();
    }

    public void clickOnCreateDocumentLink(Actions hover) throws InterruptedException {
        hover.moveToElement(createButton)
                .moveToElement(createDocumentLink)
                .click().build().perform();
    }

    public void hoverAndClickEveryCreate(WebDriver driver, Actions hover) throws InterruptedException {
        createButtonClick(hover);
        String listElements = "(.//*[@id='quickcreatetop']/ul)[2]/li";
        String lsl = listElements + "/a";
        System.out.println(lsl);
        List<WebElement> listOfCreates = driver.findElements(By.xpath(lsl));
        for (int i = 1; i <= listOfCreates.size(); i++) {
            createButtonClick(hover);
            WebElement eachCreateItem = driver.findElement(By.xpath(listElements + "[" + i + "]/a"));
            hover.moveToElement(createButton)
                    .moveToElement(eachCreateItem)
                    .click().build().perform();
        }
    }

    public boolean isDashboardLoaded(WebDriver driver, WebDriverWait wait) {
        wait.until(ExpectedConditions.visibilityOf(suiteCrmDashboard));
        return suiteCrmDashboard.isDisplayed();
    }

    public void logout(WebDriver driver, WebDriverWait wait, Actions hover) throws InterruptedException {
        hover.moveToElement(icono)
                .moveToElement(icono)
                .click().build().perform();
/*        hover.moveToElement(icono)
                .moveToElement(logout)
                .click().build().perform();
*/
        hover.moveToElement(logout).build().perform();
        logout.click();
    }

    public void addTab(WebDriverWait wait, Actions hover) throws InterruptedException {
        hover.moveToElement(actions)
                .moveToElement(actions)
                .click().build().perform();
        hover.moveToElement(actions)
                .moveToElement(addTab)
                .click().build().perform();
        wait.until(ExpectedConditions.visibilityOf(addTabWidgetButton));
        hover.moveToElement(addTabWidgetButton)
                .moveToElement(addTabWidgetButton)
                .click().build().perform();
    }

    public void search(WebDriver driver, WebDriverWait wait, Actions hover, String keyword) throws InterruptedException {
        hover.moveToElement(lupa)
                .moveToElement(lupa)
                .click().build().perform();
        wait.until(ExpectedConditions.visibilityOf(searchBox));
        searchBox.clear();
        searchBox.sendKeys(keyword);
        hover.moveToElement(iconoLupa)
                .moveToElement(iconoLupa)
                .click().build().perform();
    }

    public DashboardSaucePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
