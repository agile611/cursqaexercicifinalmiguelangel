package com.itnove.ba.crm.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by guillem on 01/03/16.
 */
public class EditAccountSaucePage {

    private WebDriver driver;

    @FindBy(xpath = ".//*[@id='pagecontent']/div[1]/h2")
    public WebElement titulo;

    @FindBy(xpath = ".//*[@id='tab-actions']/a")
    public WebElement actionsMenu;

    @FindBy(id = "delete_button")
    public WebElement deleteButton;

    public String getTitulo(){
        return titulo.getText();
    }

    public void deleteAccount(WebDriver driver, WebDriverWait wait, Actions hover) throws InterruptedException {
        hover.moveToElement(actionsMenu)
                .moveToElement(actionsMenu)
                .click().build().perform();
        hover.moveToElement(actionsMenu)
                .moveToElement(deleteButton)
                .click().build().perform();
        wait.until(ExpectedConditions.alertIsPresent());
        Alert alert = driver.switchTo().alert();
        Thread.sleep(2000);
        alert.accept();
        Thread.sleep(2000);
        driver.switchTo().defaultContent();
    }

    public EditAccountSaucePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

}
