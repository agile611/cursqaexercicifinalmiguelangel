package com.itnove.ba.crm.dashboard;

import com.itnove.ba.crm.CrmBaseSauceBrowserTest;
import com.itnove.ba.crm.pages.*;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class CreateAccountSauceTest extends CrmBaseSauceBrowserTest {

    public String accountName = UUID.randomUUID().toString();

    @Test
    public void createAccount() throws InterruptedException {
        // S'introdueix l'usuari correcte.
        // S'introdueix la contrasenya correcte.
        // Es clicka  al botó de login. 
        LoginSaucePage loginPage = new LoginSaucePage(driver);
        loginPage.login("user","bitnami");
        DashboardSaucePage dashboardPage = new DashboardSaucePage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.createButtonClick(hover);
        dashboardPage.clickOnCreateAccountLink(hover);

        CreateAccountSaucePage createAccountPage = new CreateAccountSaucePage(driver);
        createAccountPage.fillName(accountName);
        createAccountPage.saveAccount();
        EditAccountSaucePage editAccountPage = new EditAccountSaucePage(driver);
        assertEquals(accountName.toUpperCase(),editAccountPage.getTitulo().toUpperCase());
     }

    @Test(dependsOnMethods = "createAccount")
    public void deleteAccount() throws InterruptedException {
//        LoginAdminPageOp loginPage = new LoginAdminPageOp(driver);
//        loginPage.login("user","bitnami");
/*
        DashboardSaucePage dashboardPage = new DashboardSaucePage(driver);
        //Comprovo que arribo al dashboard
        assertTrue(dashboardPage.isDashboardLoaded(driver,wait));
        dashboardPage.search(driver,wait,hover,accountName);
        Thread.sleep(2000);
        SearchResultsSaucePage searchResultsPage = new SearchResultsSaucePage(driver);
        assertTrue(searchResultsPage.isSearchResultsPageLoaded(wait));
        assertEquals(searchResultsPage.isSearchKeywordCorrect(),accountName);
        searchResultsPage.clickOnFirstResult(wait);
        assertTrue(driver.getPageSource().toUpperCase().contains(accountName.toUpperCase()));
        EditAccountSaucePage editAccountPage = new EditAccountSaucePage(driver);
        assertEquals(accountName.toUpperCase(),editAccountPage.getTitulo().toUpperCase());
        editAccountPage.deleteAccount(driver, hover);
        System.out.println(accountName);
*/
        EditAccountSaucePage editAccountPage = new EditAccountSaucePage(driver);
        assertEquals(accountName.toUpperCase(),editAccountPage.getTitulo().toUpperCase());
        editAccountPage.deleteAccount(driver, wait, hover);
        System.out.println(accountName);

    }

}
